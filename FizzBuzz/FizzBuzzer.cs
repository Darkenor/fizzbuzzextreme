﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz
{
    public class FizzBuzzer
    {
        private int maxRange = 101;
        private int minRange = 1;
        private Dictionary<int, string> wordNums { get; set; }
        private StringBuilder sb = new StringBuilder();
        private string _fizz = "Fizz";
        private string _buzz = "Buzz";
        private int _modulatorOne = 3;
        private int _modulatorTwo = 5;
        public FizzBuzzer()
        {
            wordNums = new Dictionary<int, string>();
            wordNums.Add(3, _fizz);
            wordNums.Add(5, _buzz);
        }


        public void SetRange(int RangeStart, int RangeEnd)
        {
            minRange = RangeStart;
            maxRange = RangeEnd + 1;
        }

        public void SetFizzBuzz(string Fizz, string Buzz)
        {
            _fizz = Fizz;
            _buzz = Buzz;
            wordNums = new Dictionary<int, string>()
            {
                { _modulatorOne, Fizz},
                { _modulatorTwo, Buzz}
            };
        }

        public void SetIntervals(int ModulatorOne, int ModulatorTwo)
        {
            _modulatorOne = ModulatorOne;
            _modulatorTwo = ModulatorTwo;
            wordNums = new Dictionary<int, string>()
            {
                { ModulatorOne, _fizz },
                { ModulatorTwo, _buzz}
            };
        }

        public void SetNumbersAndWords(Dictionary<int, string> WordsAndNumbers)
        {
            wordNums = WordsAndNumbers;
        }

        public string FizzBuzz()
        {
            sb.Clear();
            for (int i = minRange; i < maxRange; i++)
            {
                bool match = false;
                foreach (var kp in wordNums)
                {
                    if (i % kp.Key == 0)
                    {
                        sb.Append(kp.Value.ToString());
                        match = true;
                    }
                }
                if (match)
                {
                    sb.Append(Environment.NewLine);
                    match = false;
                }
                else
                {
                    sb.AppendLine(i.ToString());
                }
            }
            return sb.ToString();
        }
    }
}