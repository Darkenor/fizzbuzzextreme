﻿using FizzBuzz;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
namespace UnitTests
{
    [TestClass]
    public class FizzBuzzTests
    {
        [TestMethod]
        public void FizzBuzz_OneToHundred()
        {
            FizzBuzzer fb = new FizzBuzzer();
            string output = fb.FizzBuzz();
            string[] results = output.Split('\n');
            Assert.AreEqual("Fizz" + "\r", results[2]);
            Assert.AreEqual("Buzz" + "\r", results[4]);
            Assert.AreEqual("Fizz" + "\r", results[98]);
            Assert.AreEqual("Buzz" + "\r", results[99]);
        }

        [TestMethod]
        public void FizzBuzz_OneToTwoHundred()
        {
            FizzBuzzer fb = new FizzBuzzer();
            fb.SetRange(1, 200);
            string output = fb.FizzBuzz();
            string[] results = output.Split('\n');
            Assert.AreEqual("Fizz" + "\r", results[2]);
            Assert.AreEqual("Buzz" + "\r", results[4]);
            Assert.AreEqual("Fizz" + "\r", results[98]);
            Assert.AreEqual("Buzz" + "\r", results[99]);
            Assert.AreEqual("Buzz" + "\r", results[199]);
        }

        [TestMethod]
        public void FizzBuzz_OneToHundred_FooBar()
        {
            FizzBuzzer fb = new FizzBuzzer();
            fb.SetFizzBuzz("Foo", "Bar");
            string output = fb.FizzBuzz();
            string[] results = output.Split('\n');
            Assert.AreEqual("Foo" + "\r", results[2]);
            Assert.AreEqual("Bar" + "\r", results[4]);
            Assert.AreEqual("Foo" + "\r", results[98]);
            Assert.AreEqual("Bar" + "\r", results[99]);
        }

        [TestMethod]
        public void FizzBuzz_OneToHundred_2and9Modulus()
        {
            FizzBuzzer fb = new FizzBuzzer();
            fb.SetFizzBuzz("Foo", "Bar");
            fb.SetIntervals(2, 9);
            string output = fb.FizzBuzz();
            string[] results = output.Split('\n');
            Assert.AreEqual("Foo" + "\r", results[1]);
            Assert.AreEqual("Foo" + "\r", results[3]);
            Assert.AreEqual("Bar" + "\r", results[98]);
        }

        [TestMethod]
        public void FizzBuzz_OneToTenMillion_2and9Modulus()
        {
            FizzBuzzer fb = new FizzBuzzer();
            fb.SetRange(1, 10000000);
            string output = fb.FizzBuzz();
            string[] results = output.Split('\n');
            Assert.AreEqual("Fizz" + "\r", results[2]);
            Assert.AreEqual("Buzz" + "\r", results[4]);
            Assert.AreEqual("Fizz" + "\r", results[98]);
            Assert.AreEqual("Buzz" + "\r", results[99]);
        }

        [TestMethod]
        public void FizzBuzz_OneToOneHundred_CustomKeyPair()
        {
            FizzBuzzer fb = new FizzBuzzer();
            var wordNums = new Dictionary<int, string>()
            {
                { 2, "Pop" },
                { 5, "Sickle"},
                { 13, "Pancakes"},
                { 97, "AreAwesome!"},
            };

            fb.SetNumbersAndWords(wordNums);
            string output = fb.FizzBuzz();
            string[] results = output.Split('\n');
            Assert.AreEqual("Pop" + "\r", results[1]);
            Assert.AreEqual("Sickle" + "\r", results[4]);
            Assert.AreEqual("Pancakes" + "\r", results[12]);
            Assert.AreEqual("AreAwesome!" + "\r", results[96]);
            Assert.AreEqual("PopSickle" + "\r", results[9]);
        }
    }
}
