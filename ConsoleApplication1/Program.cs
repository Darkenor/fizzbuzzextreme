﻿using FizzBuzz;
using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            FizzBuzzer fb = new FizzBuzzer();
            string result = fb.FizzBuzz();
            Console.Write(result);
            Console.ReadLine();
        }
    }
}
