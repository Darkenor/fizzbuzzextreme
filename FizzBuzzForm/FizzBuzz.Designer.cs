﻿namespace FizzBuzzForm
{
    partial class frmFizzBuzz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbRangeMin = new System.Windows.Forms.TextBox();
            this.tbRangeMax = new System.Windows.Forms.TextBox();
            this.tbModulusOne = new System.Windows.Forms.TextBox();
            this.tbModulusTwo = new System.Windows.Forms.TextBox();
            this.rtbFizzBuzzOutput = new System.Windows.Forms.RichTextBox();
            this.lblMinRange = new System.Windows.Forms.Label();
            this.lblMaxRange = new System.Windows.Forms.Label();
            this.lblNumOne = new System.Windows.Forms.Label();
            this.lblNumTwo = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lbKeyPairs = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbKey = new System.Windows.Forms.TextBox();
            this.tbWord = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDefault = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbRangeMin
            // 
            this.tbRangeMin.Location = new System.Drawing.Point(75, 36);
            this.tbRangeMin.Name = "tbRangeMin";
            this.tbRangeMin.Size = new System.Drawing.Size(76, 20);
            this.tbRangeMin.TabIndex = 1;
            this.tbRangeMin.Text = "1";
            // 
            // tbRangeMax
            // 
            this.tbRangeMax.Location = new System.Drawing.Point(75, 63);
            this.tbRangeMax.Name = "tbRangeMax";
            this.tbRangeMax.Size = new System.Drawing.Size(76, 20);
            this.tbRangeMax.TabIndex = 2;
            this.tbRangeMax.Text = "100";
            // 
            // tbModulusOne
            // 
            this.tbModulusOne.Location = new System.Drawing.Point(75, 89);
            this.tbModulusOne.Name = "tbModulusOne";
            this.tbModulusOne.Size = new System.Drawing.Size(76, 20);
            this.tbModulusOne.TabIndex = 3;
            this.tbModulusOne.Text = "3";
            // 
            // tbModulusTwo
            // 
            this.tbModulusTwo.Location = new System.Drawing.Point(75, 115);
            this.tbModulusTwo.Name = "tbModulusTwo";
            this.tbModulusTwo.Size = new System.Drawing.Size(76, 20);
            this.tbModulusTwo.TabIndex = 4;
            this.tbModulusTwo.Text = "5";
            // 
            // rtbFizzBuzzOutput
            // 
            this.rtbFizzBuzzOutput.Location = new System.Drawing.Point(170, 12);
            this.rtbFizzBuzzOutput.Name = "rtbFizzBuzzOutput";
            this.rtbFizzBuzzOutput.Size = new System.Drawing.Size(804, 602);
            this.rtbFizzBuzzOutput.TabIndex = 5;
            this.rtbFizzBuzzOutput.Text = "";
            // 
            // lblMinRange
            // 
            this.lblMinRange.AutoSize = true;
            this.lblMinRange.Location = new System.Drawing.Point(13, 39);
            this.lblMinRange.Name = "lblMinRange";
            this.lblMinRange.Size = new System.Drawing.Size(56, 13);
            this.lblMinRange.TabIndex = 6;
            this.lblMinRange.Text = "MinRange";
            // 
            // lblMaxRange
            // 
            this.lblMaxRange.AutoSize = true;
            this.lblMaxRange.Location = new System.Drawing.Point(13, 66);
            this.lblMaxRange.Name = "lblMaxRange";
            this.lblMaxRange.Size = new System.Drawing.Size(59, 13);
            this.lblMaxRange.TabIndex = 7;
            this.lblMaxRange.Text = "MaxRange";
            // 
            // lblNumOne
            // 
            this.lblNumOne.AutoSize = true;
            this.lblNumOne.Location = new System.Drawing.Point(13, 93);
            this.lblNumOne.Name = "lblNumOne";
            this.lblNumOne.Size = new System.Drawing.Size(49, 13);
            this.lblNumOne.TabIndex = 8;
            this.lblNumOne.Text = "NumOne";
            // 
            // lblNumTwo
            // 
            this.lblNumTwo.AutoSize = true;
            this.lblNumTwo.Location = new System.Drawing.Point(13, 118);
            this.lblNumTwo.Name = "lblNumTwo";
            this.lblNumTwo.Size = new System.Drawing.Size(50, 13);
            this.lblNumTwo.TabIndex = 9;
            this.lblNumTwo.Text = "NumTwo";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(16, 591);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(135, 23);
            this.btnCalculate.TabIndex = 10;
            this.btnCalculate.Text = "FizzBuzz!";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lbKeyPairs
            // 
            this.lbKeyPairs.FormattingEnabled = true;
            this.lbKeyPairs.Location = new System.Drawing.Point(16, 177);
            this.lbKeyPairs.Name = "lbKeyPairs";
            this.lbKeyPairs.Size = new System.Drawing.Size(135, 95);
            this.lbKeyPairs.TabIndex = 11;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(16, 306);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(135, 23);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbKey
            // 
            this.tbKey.Location = new System.Drawing.Point(16, 278);
            this.tbKey.Name = "tbKey";
            this.tbKey.Size = new System.Drawing.Size(46, 20);
            this.tbKey.TabIndex = 13;
            this.tbKey.Text = "99";
            // 
            // tbWord
            // 
            this.tbWord.Location = new System.Drawing.Point(68, 278);
            this.tbWord.Name = "tbWord";
            this.tbWord.Size = new System.Drawing.Size(83, 20);
            this.tbWord.TabIndex = 14;
            this.tbWord.Text = "Banana";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(16, 336);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(135, 23);
            this.btnClear.TabIndex = 15;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.Location = new System.Drawing.Point(16, 366);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(135, 23);
            this.btnDefault.TabIndex = 16;
            this.btnDefault.Text = "Default";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // frmFizzBuzz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 641);
            this.Controls.Add(this.btnDefault);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.tbWord);
            this.Controls.Add(this.tbKey);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbKeyPairs);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.lblNumTwo);
            this.Controls.Add(this.lblNumOne);
            this.Controls.Add(this.lblMaxRange);
            this.Controls.Add(this.lblMinRange);
            this.Controls.Add(this.rtbFizzBuzzOutput);
            this.Controls.Add(this.tbModulusTwo);
            this.Controls.Add(this.tbModulusOne);
            this.Controls.Add(this.tbRangeMax);
            this.Controls.Add(this.tbRangeMin);
            this.Name = "frmFizzBuzz";
            this.Text = "Extreme Fizz Buzz";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbRangeMin;
        private System.Windows.Forms.TextBox tbRangeMax;
        private System.Windows.Forms.TextBox tbModulusOne;
        private System.Windows.Forms.TextBox tbModulusTwo;
        private System.Windows.Forms.RichTextBox rtbFizzBuzzOutput;
        private System.Windows.Forms.Label lblMinRange;
        private System.Windows.Forms.Label lblMaxRange;
        private System.Windows.Forms.Label lblNumOne;
        private System.Windows.Forms.Label lblNumTwo;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.ListBox lbKeyPairs;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox tbKey;
        private System.Windows.Forms.TextBox tbWord;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDefault;
    }
}

