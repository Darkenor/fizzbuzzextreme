﻿using FizzBuzz;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace FizzBuzzForm
{
    public partial class frmFizzBuzz : Form
    {
        private FizzBuzzer fb = new FizzBuzzer();
        private Dictionary<int, string> wordNums;

        private BindingList<String> listBoxValues = new BindingList<string>();
        public frmFizzBuzz()
        {
            InitializeComponent();
            SetDefaultWords();
            SetWordNumbers();
            lbKeyPairs.DataSource = listBoxValues;
        }

        private void SetDefaultWords()
        {
            wordNums = new Dictionary<int, string>()
            {
                { 3, "Fizz" },
                { 5, "Buzz" }
            };
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            rtbFizzBuzzOutput.Clear();
            SetNumbers();
            SetWordNumbers();
            rtbFizzBuzzOutput.Text = fb.FizzBuzz();
        }

        private int GetValueFromTextBox(TextBox Box)
        {
            int returnVal =0;
            Int32.TryParse(Box.Text, out returnVal);
            if (returnVal==0)
            {
                MessageBox.Show(Box.Name + " is not in an acceptable format");
                return 1;
            }
            return returnVal;
        }

        private void SetNumbers()
        {
            int modOne = GetValueFromTextBox(tbModulusOne);
            int modTwo = GetValueFromTextBox(tbModulusTwo);
            int rangeMin = GetValueFromTextBox(tbRangeMin);
            int rangeMax = GetValueFromTextBox(tbRangeMax);
            fb.SetRange(rangeMin, rangeMax);
            fb.SetIntervals(modOne, modTwo);
        }

        private void SetWordNumbers()
        {
            listBoxValues.Clear();

            foreach (var wn in wordNums)
            {
                listBoxValues.Add(wn.Key.ToString() + ", " + wn.Value);
            }
            fb.SetNumbersAndWords(wordNums);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int key = GetValueFromTextBox(tbKey);
            string value = tbWord.Text;
            if (!wordNums.ContainsKey(key))
            {
                wordNums.Add(key, value);
                SetWordNumbers();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            wordNums = new Dictionary<int, string>();
            SetWordNumbers();
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            SetDefaultWords();
            SetWordNumbers();
        }
    }
}
